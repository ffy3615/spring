package cn.edu.scujcc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/channel")
public class ChannelController {
	@Autowired
	private ChannelService service;
	
	@PostMapping
	public Channel createChannel(@RequestBody Channel c) {
		Channel result = null;
		result = service.createChannel(c);
		return result;
	}
	
	@PutMapping
	public Channel updateChannel(@RequestBody Channel c) {
		Channel result = null;
		result = service.updateChannel(c);
		return result;
	}
	
	@GetMapping
	public List<Channel> getAllChannel() {
		List<Channel> result = null;
		result = service.getAllChannel();
		return result;
	}
	
	@GetMapping("/{id}")
	public Channel getChannel(@PathVariable String id) {
		Channel result = null;
		result = service.getChannel(id);
		return result;
	}
	
	@DeleteMapping("/{id}")
	public boolean deleteChannel(@PathVariable String id) {
		boolean result = false;
		result = service.deleteChannel(id);
		return result;
	}
	
	@GetMapping("/search/{desc}")
	public List<Channel> searchChannel(@PathVariable String desc){
		List<Channel> result = null;
		result = service.searchChannel(desc);
		return result;
	}
}
