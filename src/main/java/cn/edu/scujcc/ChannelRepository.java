package cn.edu.scujcc;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChannelRepository extends MongoRepository<Channel, String> {
	public List<Channel> findByDesc(String desc);
}
