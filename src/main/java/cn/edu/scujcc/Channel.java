package cn.edu.scujcc;

import org.springframework.data.annotation.Id;

/**
 * 频道实体对象
 * @author DELL
 *
 */

public class Channel {
	@Id
	private String id;
	private String name;
	private String desc;
	private String url;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
