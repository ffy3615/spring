package cn.edu.scujcc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChannelsrvApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChannelsrvApplication.class, args);
	}

}
